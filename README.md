# MicroShift Robotics Operation Demos

本リポジトリは、Red Hat Summit Connect | Japan 2022でエッジコンピューティングの展示用に作成したものです。本リポジトリには、以下の5つのデモが含まれています。メインのデモは「4. NVIDIA Jetson AGX OrinでNVIDIA Riva APIサーバを実行し、声でPiCar-Vを操縦」です。1〜3のデモは4を実装するまでに試したサンプルとしてご利用ください。

## [1. SunFounder PiCar-Vの遠隔操縦プログラムをMicroShiftで実行](source/picar-demo/demo1)

[SunFounder Smart Video Car Kit V2.0](https://docs.sunfounder.com/projects/picar-v/ja/latest/)は、Raspberry pi4を使用してロボットの操作を学習するためのラズパイロボットです。
このデモは、SunFounderの提供する「remote_control」サンプルプログラムをコンテナ/MicroShiftでも動作する様に一部改修して実装しています。

オリジナルのGitリポジトリは以下を参照してください。
https://github.com/sunfounder/SunFounder_PiCar-V/tree/master/remote_control

## [2. SunFounder PiCar-Vの赤いボール追跡プログラムをMicroShiftで実行](source/picar-demo/demo2)


オリジナルのGitリポジトリは以下です。
https://github.com/sunfounder/SunFounder_PiCar-V/tree/master/ball_track

## [3. NVIDIA Jetson AGX OrinでのDeepStream SDKのサンプルをMicroShiftで実行](source/jetson-demo)

NVIDIA Jetson AGX OrionへDeepStream SDKのサンプルコードのObject Detection(SSD, Yolo v3)をデプロイします。

## [4. NVIDIA Jetson AGX OrinでNVIDIA Riva APIサーバを実行し、声でPiCar-Vを操縦](source/picar-jetson-integration-demo)

NVIDIA Jetson AGX Orion上に[Riva API](https://docs.nvidia.com/deeplearning/riva/user-guide/docs/quick-start-guide.html)をデプロイし、音声認識を結果を元にPiCar-Vの操縦APIを実行します。

![声でPiCar-V操作デモ](images/ASR-based_picar_operation.png) 

## [5. タミヤカスタムプログラミングロボットへRaspberry Pi2を搭載し、Podmanで遠隔操縦プログラムを実行](source/piconzero)

![カムロボット操作デモ](images/Custom_Robot_and_Podman.png) 

# 環境
- SunFounder Smart Video Car Kit V2.0
- Raspberry pi4B
  - OS: raspbian OS(64bit)
- Raspberry pi 2B
  - OS: raspbian OS(32bit)
- NVIDIA Jetson AGX Orion
  - OS: Ubuntu 20.04
- コンテナレジストリ:
  - registry.gitlab.com/yono1/microshift-picar-demo
  - quay.io/yono/mynvidia-riva-test
- Gitリポジトリ
  - https://gitlab.com/yono1/microshift-picar-demo
  - https://github.com/yd-ono/SunFounder_PiCar.git
PiCar-Vのfork。Kubernetes上でPiCarのサンプルアプリケーションを実行するためにソースコードを一部修正


# 準備

①/etc/hostsを編集しておく（念の為）

```
100.64.1.76 picar picar-web-picar.cluster.local picar-camera-picar.cluster.local skupper-picar.cluster.local microshift.local
100.64.1.77 jetson nano.local microshift-cam-reg.local skupper-riva-api.cluster.local
100.64.1.84 cumrobot cumrobot.local
```


②WiFi設定

```
$ nmcli device wifi connect [SSID] password [PASSWORD]
```

```
$ sudo cat /etc/wpa_supplicant/wpa_supplicant.conf 
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
	ssid="$SSID"
	psk=$PASSWORD
}
```

③manifestのhostAliases箇所を修正
Ingressの名前解決の関係でhostAliasesで対応してる。

manifest/picar-jetson-integration-demo/skupper/jetson/skupper-controller.yaml
manifest/picar-jetson-integration-demo/skupper/jetson/skupper-router.yaml
manifest/picar-jetson-integration-demo/skupper/picar/skupper-controller.yaml
manifest/picar-jetson-integration-demo/skupper/picar/skupper-router.yaml

- Podの再作成
- skupper linkの再作成


```
$ skupper token create tojetson.yaml --expiry 20160m0s
$ skupper link create tojetson.yaml
```

- deployment/picarのexpose
- skupperのデプロイはマニフェストで行わない！（skupper initした後にdeploymentのimageを差し替える。そうしないとバグる。）
- あと、ingressのdnsがないので、hostAliasesでやってる...汗

```
$ skupper expose deployment/picar 
```


④Hubクラスタの設定変更

https://console-openshift-console.apps.demo.sandbox601.opentlc.com/
id: kubeadmin
pw: 

```
$ oc project picar-v
$ oc edit managedcluster picar-v
spec:
  hubAcceptsClient: true
  managedClusterClientConfigs:
  - url: https://xxx.xxx.xxx.xxx:6443

$ oc project jetson
$ oc edit managedcluster jetson
```

# GUI画面

●PiCar-Vの操作画面
picar-web-picar.cluster.local

●カムロボットの管理画面
http://cumrobot:8000/run/?action=setup

●Skupperのコンソール画面
skupper-picar.cluster.local microshift.local

●OpenShiftの管理画面
https://console-openshift-console.apps.demo.sandbox601.opentlc.com/

# デバイスを再起動したら

①②③④はKubernetesで自動起動します。

②JetsonのDeepstream SDKはDisplayとのコネクションがないとこけます。

困ったら、xhostの許可設定を与えてください。

```
$ xhost +
```

⑤podman
```
$ systemctl status --user container-piconzero.service
 active (running)

$ systemctl start --user container-piconzero.service

$ podman ps -a
CONTAINER ID  IMAGE                                                          COMMAND  CREATED         STATUS             PORTS                   NAMES
76f79ad0f541  registry.gitlab.com/yono1/microshift-picar-demo/piconzero:1.0  ./start  46 minutes ago  Up 18 seconds ago  0.0.0.0:8000->8000/tcp  piconzero
```

# Deploy方法
## ①Sunfounder PiCar-Vの制御プログラム(python3)をMicroShiftで動かす

注意: デバイス上の/dev/usbをマウントしてますので、Demo1からDemo2へ切り替える際は、Demo1のPodが消えるまでDemo2は失敗します。

### Demo1. リモート制御(キーボード操作)

```
$ oc apply -f microshift-picar-demo/manifest/picar-demo/demo1
```

#### Demo2. PiCar-Vが赤いボールを追っかける

```
$ oc apply -f microshift-picar-demo/manifest/picar-demo/demo2
```

## ②NVIDIA Jetson AGX OrionのDeepStream SDKをMicroShiftで動かす

DeepStream SDKのサンプルアプリケーションはX11アプリケーションのため、ディスプレイ接続が必須です。
そのため、環境変数 DISPLAYが存在しないと起動に失敗します。

以下のコマンドでアプリケーションを起動すると、MLモデルの初期化(数分)を行ったのち、
Jetsonが接続するディスプレイにX11アプリケーションが起動します。

SSDの場合
```
$ oc apply -f microshift-picar-demo/manifest/jetson-demo/common
$ oc apply -f microshift-picar-demo/manifest/jetson-demo/object_detection_ssd
```

Yolo v3の場合
```
$ oc apply -f microshift-picar-demo/manifest/jetson-demo/common
$ oc apply -f microshift-picar-demo/manifest/jetson-demo/object_detection_yolo
```

## ③NVIDIA Jetson AGX Orion上のRivaで音声認識を行い、PiCAR-Vを制御する

## ④タミヤのカムロボット w/Raspberry Pi2Bの遠隔制御プログラム(python3)をPodman上で動かす

---
# Tips
## 1. ラズパイのコンテナイメージを作成
```
$ wget https://downloads.raspberrypi.org/raspios_lite_arm64/root.tar.xz
$ docker image import root.tar.xz raspios_lite_arm64:bullseye
```

## 2. PiCar-Vのremote-commandを実行する場合はホスト側でファイアウォールの穴あけが必要

```
$ firewall-cmd --add-port=8000/tcp --add-port=8765/tcp --permanent
$ firewall-cmd --reload
```

2. remote-controllerコンテナのビルド

```
$ cd remote-controller
$ docker build . -t remote-controller:1.0
```

```
$ docker run -it --rm --privileged -p 8000:8000 -p 8765:8765 remote-control:1.0
```

3. ball_trackerコンテナのビルド

```
$ cd ball_tracker
$ docker build . -t ball_tracker:1.0
```

```
$ docker run -it --rm --privileged ball_tracker:1.0
```


環境変数
SERVICE_ENDPOIN remote-controllerのviewのエンドポイントIP。0.0.0.0でok
SERVICE_PORT デフォルトで8000/tcp 
CAMERA_ENDPOINT Webカメラ映像取得のためのエンドポイントIP。0.0.0.0だとこける。ラズパイのIPアドレスを指定
CAMERA_PORT デフォルトで8765/tcp(JStreamer)

## raspbianOSへのMicroShiftのインストール


# ACMでMicroShiftを管理する
https://access.redhat.com/terms-based-registry/ でService Accountを取得する

```
oc project open-cluster-management-agent
oc apply -f open-cluster-management-image-pull-credentials.yaml
oc secrets link klusterlet open-cluster-management-image-pull-credentials --for=pull

oc project open-cluster-management-agent-addon 
oc apply -f open-cluster-management-image-pull-credentials.yaml
oc secrets link cluster-proxy open-cluster-management-image-pull-credentials --for=pull
oc secrets link klusterlet-registration-sa open-cluster-management-image-pull-credentials --for=pull
oc secrets link klusterlet-work-sa open-cluster-management-image-pull-credentials --for=pull

```

```
$ oc get pods -A | grep open-cluster
open-cluster-management-agent-addon   application-manager-56d746984c-l9fvs             1/1     Running   0          25m
open-cluster-management-agent-addon   cluster-proxy-proxy-agent-658977c575-hl48v       2/2     Running   0          25m
open-cluster-management-agent-addon   cluster-proxy-proxy-agent-658977c575-l6cd7       2/2     Running   0          25m
open-cluster-management-agent-addon   klusterlet-addon-search-76fd8c55b7-sgqnb         1/1     Running   0          25m
open-cluster-management-agent-addon   klusterlet-addon-workmgr-5fbd7dc84-4gjvq         1/1     Running   0          25m
open-cluster-management-agent         klusterlet-7b8848db79-n82s4                      1/1     Running   0          26m
open-cluster-management-agent         klusterlet-registration-agent-56ff9dd88f-rzcjz   1/1     Running   0          26m
open-cluster-management-agent         klusterlet-work-agent-548594f75f-k4pbn           1/1     Running   0          25m
```

https://github.com/yd-ono/application-samples/tree/main/pacman


# TIPS
コンソール接続
minicom -D /dev/cu.usbmodemTOPO1D6EC0B9* -8 -b 115200

ラズパイ WiFi設定(wpa_supplicant)
$ sudo sh -c 'wpa_passphrase ssid パスワード | grep -v "#psk=" >> /etc/wpa_supplicant/wpa_supplicant.conf'
$ sudo reboot

WiFi設定(NetworkManager)
nmcli d wifi connect $SSID password $PASSWORD

SkupperのARM版ビルド

skupper-router
```
$ git clone https://github.com/skupperproject/skupper-router -b 2.1.x
$ cd skupper-router
$ docker build -t registry.gitlab.com/yono1/microshift-picar-demo/skupper/skupper-router:2.1.0 .
```

skupper
```
$ git clone https://github.com/skupperproject/skupper.git -b 1.1
$ cd skupper
$ make docker-build
```

# ライセンス
本デモは、SunFounder Smart Video Car Kit V2.0のサンプルプログラムを一部使用しています。それらのコードは、SunFounderのGNU General Public License のバージョン 2 またはそれ以降のバージョンのいずれかに従って、このプログラムを再配布または変更することができます。

