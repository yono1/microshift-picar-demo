
# Hub Cluster

```
$ oc login
$ export CLUSTER_NAME = jetson
$ oc label namespace ${CLUSTER_NAME} cluster.open-cluster-management.io/managedCluster=${CLUSTER_NAME}
$ oc apply -f managed-cluster.yaml
$ oc apply -f klusterlet-addon-config.yaml
$ oc get secret ${CLUSTER_NAME}-import -n ${CLUSTER_NAME} -o jsonpath={.data.crds\\.yaml} | base64 --decode > ../managedcluster/klusterlet-crd.yaml
$ oc get secret ${CLUSTER_NAME}-import -n ${CLUSTER_NAME} -o jsonpath={.data.import\\.yaml} | base64 --decode > ../managedcluster/import.yaml
```

```
$ oc get managedcluster
NAME            HUB ACCEPTED   MANAGED CLUSTER URLS                           JOINED   AVAILABLE   AGE
jetson          true           https://100.64.1.77:6443                       True     True        18m
local-cluster   true           https://api.demo.sandbox601.opentlc.com:6443   True     True        5h39m
picar-v         true           https://100.64.1.76:6443                       True     True        88m

$ oc edit managedcluster jetson
spec:
  hubAcceptsClient: true
  managedClusterClientConfigs:
  - url: https://100.64.1.77:6443
```
