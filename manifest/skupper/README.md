

# picar
```
$ skupper init
$ oc edit deploy skupper-router
image: registry.gitlab.com/yono1/microshift-picar-demo/skupper/skupper-router:2.1.0
image: registry.gitlab.com/yono1/microshift-picar-demo/skupper/config-sync:1.1.0

    spec:
      hostAliases:
      - ip: "100.64.1.77"
        hostnames:
        - "skupper-edge-riva-client.cluster.local"
        - "skupper-riva-client.cluster.local"
        - "skupper-inter-router-riva-client.cluster.local"
        - "claims-riva-client.cluster.local"
      containers:
      - env:

$ oc edit deploy skupper-service-controller
image: registry.gitlab.com/yono1/microshift-picar-demo/skupper/service-controller:1.1.0
    spec:
      hostAliases:
      - ip: "100.64.1.77"
        hostnames:
        - "skupper-edge-riva-client.cluster.local"
        - "skupper-riva-client.cluster.local"
        - "skupper-inter-router-riva-client.cluster.local"
        - "claims-riva-client.cluster.local"
      containers:
      - env:

$ oc apply -f skupper-route.yaml
```

# riva-client
```
$ skupper init
$ oc edit deploy skupper-router
image: registry.gitlab.com/yono1/microshift-picar-demo/skupper/skupper-router:2.1.0
image: registry.gitlab.com/yono1/microshift-picar-demo/skupper/config-sync:1.1.0
      hostAliases:
      - ip: "100.64.1.76"
        hostnames:
        - "skupper-edge-picar.cluster.local"
        - "skupper-picar.cluster.local"
        - "skupper-inter-router-picar.cluster.local"
        - "claims-picar.cluster.local"

$ oc edit deploy skupper-service-controller
image: registry.gitlab.com/yono1/microshift-picar-demo/skupper/service-controller:1.1.0
      hostAliases:
      - ip: "100.64.1.76"
        hostnames:
        - "skupper-edge-picar.cluster.local"
        - "skupper-picar.cluster.local"
        - "skupper-inter-router-picar.cluster.local"
        - "claims-picar.cluster.local"

$ oc apply -f skupper-route.yaml
```

# Link
```
[jetson側]
$ skupper token create tojetson.yaml --expiry 20160m0s

[picar側]
$ skupper link create tojetson.yaml
```

# コンソールのパスワード
```
$ oc get secret skupper-console-users -o json | jq .data.admin -r | base64 -d
```