# NVIDIA Riva & PiCar-V Integration

import argparse
import os
import queue
import sys
import requests

import grpc
import pyaudio
import riva_api.riva_asr_pb2 as rasr
import riva_api.riva_asr_pb2_grpc as rasr_srv
import riva_api.riva_audio_pb2 as ra


# サンプリング周波数
RATE = os.getenv('RATE')
if RATE:
    RATE = int(RATE)
elif not RATE:
    RATE = 16000

CHUNK = int(RATE / 10)  # 100ms

PICAR_ENDPOINT = os.getenv('PICAR_ENDPOINT')

# PiCAR-Vを制御するためのAPIのエンドポイント
if PICAR_ENDPOINT:
    PICAR_URL = "http://" + PICAR_ENDPOINT + "/run/?"
elif not PICAR_ENDPOINT:
    PICAR_URL = "http://localhost/run/?"

# HTTPコネクションタイムアウト
CONNECTION_TIMEOUT = os.getenv('CONNECTION_TIMEOUT')
if CONNECTION_TIMEOUT:
    CONNECTION_TIMEOUT = float(CONNECTION_TIMEOUT)
elif not CONNECTION_TIMEOUT:
    CONNECTION_TIMEOUT = 2.0

# HTTPリードタイムアウト
READ_TIMEOUT = os.getenv('READ_TIMEOUT')
if READ_TIMEOUT:
    READ_TIMEOUT = float(READ_TIMEOUT)
elif not READ_TIMEOUT:
    READ_TIMEOUT = 4.0


# 発話内容に応じたPiCar-Vへの指示辞書
action_dic = {
        "go":"action=forward",
        "goal":"action=forward",
        "forward":"action=forward",
        "back":"action=backward",
        "buck":"action=backward",
        "right":"action=fwright",
        "light":"action=fwright",
        "goright":"action=fwright",
        "golight":"action=fwright",
        "left":"action=fwleft",
        "goleft":"action=fwleft",
        "stop":"action=stop",
        "up":"action=camup",
        "down":"action=camdown",
        "rightcamera":"action=camright",
        "lightcamera":"action=camright",
        "leftcamera":"action=camleft"
        }


def get_args():
    parser = argparse.ArgumentParser(description="Streaming transcription via Riva AI Services")
    parser.add_argument("--server", default="localhost:50051", type=str, help="URI to GRPC server endpoint")
    parser.add_argument("--input-device", type=int, default=None, help="output device to use")
    parser.add_argument("--language-code", default="en-US", type=str, help="Language code of the model to be used")
    parser.add_argument("--ssl_cert", type=str, default="", help="Path to SSL client certificatates file")
    parser.add_argument(
        "--use_ssl", default=False, action='store_true', help="Boolean to control if SSL/TLS encryption should be used"
    )
    return parser.parse_args()


class MicrophoneStream(object):
    """Opens a recording stream as a generator yielding the audio chunks."""

    def __init__(self, rate, chunk, device=None):
        self._rate = rate
        self._chunk = chunk
        self._device = device

        # Create a thread-safe buffer of audio data
        self._buff = queue.Queue()
        self.closed = True

    def __enter__(self):
        self._audio_interface = pyaudio.PyAudio()
        self._audio_stream = self._audio_interface.open(
            format=pyaudio.paInt16,
            input_device_index=self._device,
            channels=1,
            rate=self._rate,
            input=True,
            frames_per_buffer=self._chunk,
            stream_callback=self._fill_buffer,
        )

        self.closed = False

        return self

    def __exit__(self, type, value, traceback):
        self._audio_stream.stop_stream()
        self._audio_stream.close()
        self.closed = True
        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.
        self._buff.put(None)
        self._audio_interface.terminate()

    def _fill_buffer(self, in_data, frame_count, time_info, status_flags):
        """Continuously collect data from the audio stream, into the buffer."""
        self._buff.put(in_data)
        return None, pyaudio.paContinue

    def generator(self):
        while not self.closed:
            chunk = self._buff.get()
            if chunk is None:
                return
            data = [chunk]

            while True:
                try:
                    chunk = self._buff.get(block=False)
                    if chunk is None:
                        return
                    data.append(chunk)
                except queue.Empty:
                    break

            yield b''.join(data)

## Rivaで解析した発話内容に応じたPiCar-Vの動作制御
def robot_action(responses):
    num_chars_printed = 0

    for response in responses:
        if not response.results:
            continue

        partial_transcript = ""
        for result in response.results:
            if not result.alternatives:
                continue

            transcript = result.alternatives[0].transcript

            if not result.is_final:
                partial_transcript += transcript
                tmp = partial_transcript.strip()
                tmp = partial_transcript.strip(".")
                result = tmp.lower()
                if result in action_dic:
                    action = action_dic[result]
                    print( "Action Partial> " + transcript + " : " + PICAR_URL + action + "\r")
                    try:
                        r = requests.get( PICAR_URL + action, timeout=(CONNECTION_TIMEOUT, READ_TIMEOUT) )
                        r.raise_for_status()
                    except requests.exceptions.RequestException as e:
                        print("error: ",e)
            else:
                overwrite_chars = ' ' * (num_chars_printed - len(transcript))
                tmp = transcript + overwrite_chars
                tmp = tmp.strip()
                tmp = tmp.strip(".")
                result = tmp.lower()
                if result in action_dic:
                    action = action_dic[result]
                    print( "Action Result> " + transcript + " : " + PICAR_URL + action + "\r")
                    try:
                        r = requests.get( PICAR_URL + action, timeout=(CONNECTION_TIMEOUT,READ_TIMEOUT) )
                        r.raise_for_status()
                    except requests.exceptions.RequestException as e:
                        print("error: ",e)
                else:
                    print( "Action Result> " + transcript + "\r" )
                num_chars_printed = 0

def main():
    args = get_args()


    if args.ssl_cert != "" or args.use_ssl:
        root_certificates = None
        if args.ssl_cert != "" and os.path.exists(args.ssl_cert):
            with open(args.ssl_cert, 'rb') as f:
                root_certificates = f.read()
        creds = grpc.ssl_channel_credentials(root_certificates)
        channel = grpc.secure_channel(args.server, creds)
    else:
        channel = grpc.insecure_channel(args.server)

    client = rasr_srv.RivaSpeechRecognitionStub(channel)

    config = rasr.RecognitionConfig(
        encoding=ra.AudioEncoding.LINEAR_PCM,
        sample_rate_hertz=RATE,
        language_code=args.language_code,
        max_alternatives=1,
        enable_automatic_punctuation=True,
    )
    streaming_config = rasr.StreamingRecognitionConfig(config=config, interim_results=True)

    with MicrophoneStream(RATE, CHUNK, device=args.input_device) as stream:
        audio_generator = stream.generator()
        requests = (rasr.StreamingRecognizeRequest(audio_content=content) for content in audio_generator)

        def build_generator(cfg, gen):
            yield rasr.StreamingRecognizeRequest(streaming_config=cfg)
            for x in gen:
                yield x

        responses = client.StreamingRecognize(build_generator(streaming_config, requests))

        robot_action(responses)


if __name__ == '__main__':
    main()
