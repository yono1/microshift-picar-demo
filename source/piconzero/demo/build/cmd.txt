#!/bin/bash

## build container
podman build -t registry.gitlab.com/yono1/microshift-picar-demo/piconzero:1.0 .

## run the container
podman run -d --device /dev/i2c-1:/dev/i2c-1 -p 8000:8000 --annotation run.oci.keep_original_groups=1 --restart always registry.gitlab.com/yono1/microshift-picar-demo/piconzero:1.0

# access to http://cumrobot:8000/run/?action=setup