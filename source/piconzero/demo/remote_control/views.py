from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse
from .driver import piconzero as pz


is_setup = False

def setup():
	global SPEED, is_setup
	if is_setup == True:
		return
	SPEED = 60
	is_setup = True

def run(request):
	pz.init()
	global SPEED
	
	if 'action' in request.GET:
		action = request.GET['action']
		if action == 'setup':
			setup()
		elif action == 'forward':
			pz.forward(SPEED)
		elif action == 'backward':
			pz.reverse(SPEED)
		elif action == 'right':
			pz.spinRight(SPEED)
		elif action == 'left':
			pz.spinLeft(SPEED)
		elif action == 'stop':
			pz.stop()

	if 'speed' in request.GET:
		speed = int(request.GET['speed'])
		if speed < 0:
			speed = 0
		if speed > 100:
			speed = 100
		SPEED = speed
	return render(request,'run.html')
