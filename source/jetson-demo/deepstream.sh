#!/bin/bash

sudo docker run -it --rm --runtime nvidia -e DISPLAY=$DISPLAY -v /home/microshift/Docker/deepstream_app_config_ssd.txt:/opt/nvidia/deepstream/deepstream-6.1/sources/objectDetector_ssd/deepstream_app_config_ssd.txt -v /tmp/.X11-unix/:/tmp/.X11-unix -v /dev/video0:/dev/video0 --privileged registry.gitlab.com/yono1/microshift-picar-demo/object_detection_jetson:ssd deepstream-app -c deepstream_app_config_ssd.txt
