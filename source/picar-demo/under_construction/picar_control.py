from picar import front_wheels, back_wheels
from picar.SunFounder_PCA9685 import Servo
import picar
from time import sleep
import cv2
import numpy as np
import picar
import os
from kafka import KafkaConsumer

picar.setup()
kernel = np.ones((5,5),np.uint8)

SCREEN_WIDTH = 160
SCREEN_HIGHT = 120
CENTER_X = SCREEN_WIDTH/2
CENTER_Y = SCREEN_HIGHT/2
BALL_SIZE_MIN = SCREEN_HIGHT/10
BALL_SIZE_MAX = SCREEN_HIGHT/3

# Filter setting, DONOT CHANGE
hmn = 12
hmx = 37
smn = 96
smx = 255
vmn = 186
vmx = 255

CAMERA_STEP = 2
CAMERA_X_ANGLE = 20
CAMERA_Y_ANGLE = 20

MIDDLE_TOLERANT = 5
PAN_ANGLE_MAX   = 170
PAN_ANGLE_MIN   = 10
TILT_ANGLE_MAX  = 150
TILT_ANGLE_MIN  = 70
FW_ANGLE_MAX    = 90+30
FW_ANGLE_MIN    = 90-30

SCAN_POS = [[20, TILT_ANGLE_MIN], [50, TILT_ANGLE_MIN], [90, TILT_ANGLE_MIN], [130, TILT_ANGLE_MIN], [160, TILT_ANGLE_MIN], 
            [160, 80], [130, 80], [90, 80], [50, 80], [20, 80]]

bw = back_wheels.Back_Wheels()
fw = front_wheels.Front_Wheels()
pan_servo = Servo.Servo(1)
tilt_servo = Servo.Servo(2)
picar.setup()

fw.offset = 0
pan_servo.offset = 10
tilt_servo.offset = 0

bw.speed = 0
fw.turn(90)
pan_servo.write(90)
tilt_servo.write(90)

motor_speed = 40

# Kafka Topic
topic = "video"

KAFKA_SERVER = os.getenv('KAFKA_SERVER')
if KAFKA_SERVER:
    KAFKA_SERVER = KAFKA_SERVER + '9092'
elif not KAFKA_SERVER:
    KAFKA_SERVER = 'localhost:9092'

def main():
    pan_angle = 90              # initial angle for pan
    tilt_angle = 90             # initial angle for tilt
    fw_angle = 90

    scan_count = 0

    print("Begin!")
    while True:
        x = 0             # x initial in the middle
        y = 0             # y initial in the middle
        r = 0             # ball radius initial to 0(no balls if r < ball_size)

        for _ in range(10):
            (tmp_x, tmp_y), tmp_r = find_blob()
            if tmp_r > BALL_SIZE_MIN:
                x = tmp_x
                y = tmp_y
                r = tmp_r
                break

        print(r, BALL_SIZE_MIN, BALL_SIZE_MAX)

        # scan:
        if r < BALL_SIZE_MIN:
            bw.stop()
            #sleep(0.1)
            print("STOP [r < BALL_SIZE_MIN]")
            
        elif r < BALL_SIZE_MAX:
            delta_x = CENTER_X - x
            delta_y = CENTER_Y - y
            delta_pan = int(float(CAMERA_X_ANGLE) / SCREEN_WIDTH * delta_x)
            pan_angle += delta_pan
            delta_tilt = int(float(CAMERA_Y_ANGLE) / SCREEN_HIGHT * delta_y)
            tilt_angle += delta_tilt

            if pan_angle > PAN_ANGLE_MAX:
                pan_angle = PAN_ANGLE_MAX
            elif pan_angle < PAN_ANGLE_MIN:
                pan_angle = PAN_ANGLE_MIN
            if tilt_angle > TILT_ANGLE_MAX:
                tilt_angle = TILT_ANGLE_MAX
            elif tilt_angle < TILT_ANGLE_MIN:
                tilt_angle = TILT_ANGLE_MIN
            
            pan_servo.write(pan_angle)
            tilt_servo.write(tilt_angle)
            #sleep(0.01)

            fw_angle = 180 - pan_angle
            if fw_angle < FW_ANGLE_MIN or fw_angle > FW_ANGLE_MAX:
                fw_angle = ((180 - fw_angle) - 90)/2 + 90
                fw.turn(fw_angle)
                bw.speed = motor_speed
                bw.backward()
                print("BACK r < BALL_SIZE_MAX && fw_angle < FW_ANGLE_MIN or fw_angle > FW_ANGLE_MAX")
            else:
                fw.turn(fw_angle)
                bw.speed = motor_speed
                bw.forward()
                print("FORWARD r < BALL_SIZE_MAX")
        else:
            bw.stop()
            print("STOP r > BALL_SIZE_MAX")
        
def destroy():
    bw.stop()
    img.release()

def find_blob() :
    radius = 0

    consumer = KafkaConsumer(
        topic,
        bootstrap_servers=[KAFKA_SERVER])

    # Load input image
    for msg in consumer:

        bgr_image = np.frombuffer(msg.value, dtype=np.uint8)
        bgr_image = cv2.imdecode(bgr_image, cv2.IMREAD_UNCHANGED)
        orig_image = bgr_image
        bgr_image = cv2.medianBlur(bgr_image, 3)

        # Convert input image to HSV
        hsv_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2HSV)

        # Threshold the HSV image, keep only the red pixels
        lower_red_hue_range = cv2.inRange(hsv_image, (0, 100, 100), (10, 255, 255))
        upper_red_hue_range = cv2.inRange(hsv_image, (160, 100, 100), (179, 255, 255))

        # Combine the above two images
        red_hue_image = cv2.addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0)
        red_hue_image = cv2.GaussianBlur(red_hue_image, (9, 9), 2, 2)

        # Use the Hough transform to detect circles in the combined threshold image
        circles = cv2.HoughCircles(red_hue_image, cv2.HOUGH_GRADIENT, 1, 120, 100, 20, 10, 0)
    
        if circles is not None:
            circles = np.uint16(np.around(circles))
 
        # Loop over all detected circles and outline them on the original image
            all_r = np.array([])

        # print("circles: %s"%circles)
            try:
                for i in circles[0,:]:
                    # print("i: %s"%i)
                    all_r = np.append(all_r, int(round(i[2])))
                closest_ball = all_r.argmax()
                center=(int(round(circles[0][closest_ball][0])), int(round(circles[0][closest_ball][1])))
                radius=int(round(circles[0][closest_ball][2]))
            except IndexError:
                pass

        k = cv2.waitKey(5) & 0xFF
        if k == 27:
            return (0, 0), 0
        if radius > 3:
            return center, radius
        else:
            return (0, 0), 0


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        destroy()
