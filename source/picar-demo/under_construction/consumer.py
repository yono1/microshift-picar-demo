from flask import Flask, Response
from kafka import KafkaConsumer
import numpy as np
import cv2
from skimage.metrics import structural_similarity as ssim

# Fire up the Kafka Consumer
topic = "video"

KAFKA_SERVER = os.getenv('KAFKA_SERVER')
if KAFKA_SERVER:
    KAFKA_SERVER = KAFKA_SERVER + '9092'
elif not KAFKA_SERVER:
    KAFKA_SERVER = 'localhost:9092'

consumer = KafkaConsumer(
    topic,
    bootstrap_servers=[KAFKA_SERVER])


# Set the consumer in a Flask App
app = Flask(__name__)

@app.route('/video', methods=['GET'])
def video():
    return Response(
        get_video_stream(),
        mimetype='multipart/x-mixed-replace; boundary=frame')

def get_video_stream():
    
    msg_buff = []
    for msg in consumer:
        input_img = np.frombuffer(msg.value, dtype=np.uint8)

        # decode msg
        input_img = cv2.imdecode(input_img, -1)
    
        ret, buffer = cv2.imencode('.jpg', input_img)

        yield (b'--frame\r\n'
               b'Content-Type: image/jpg\r\n\r\n' + buffer.tobytes() + b'\r\n\r\n') 
